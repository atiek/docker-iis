docker-iis

# ️前提

* 以下の設定がされたWindows Server 2016
    * WinRM有効
        * 参考: AnsibleでWindowsを操作する準備をする - Qiita
            * http://qiita.com/yunano/items/f9d5652a296931a09a70
    * gitインストール
    * dockerインストール
* 対象サーバ
    * http://13.78.82.39/
    * http://13.78.82.39:8080/

# リポジトリセットアップ

gitlabのvariableに以下の値を設定してください(https://gitlab.com/mijs2016-iac/docker-iis/variables)

* ANSIBLE_USER: Windows ServerのWinRM ユーザ名
* ANSIBLE_PASSWORD: Windows ServerのWinRM パスワード